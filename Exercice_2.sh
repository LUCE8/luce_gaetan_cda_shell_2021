#!/bin/bash

# Ce programme affiche ligne par ligne les valeurs entrées en arguments grâce à la commande shift

PAR1=$1
PAR2=$2
PAR3=$3
PAR4=$4
PAR5=$5

if [ $# -eq 0 ]
then
	echo "Sans argument"
else
	while (($#))
	do
		echo $1
		shift
	done
fi


