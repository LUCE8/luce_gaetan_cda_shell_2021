#!/bin/bash

VAR1=0
VAR2=0

echo "Entrer le nom du groupe"
read VAR1

VAR2=$(grep $VAR1 /etc/group | cut -d: -f1)
[ -z $VAR2 ]

if [ $? -eq 0 ]
then
	        echo "Le groupe $VAR1 n'existe pas "
	else
		        echo "Le groupe $VAR1 existe "
fi
