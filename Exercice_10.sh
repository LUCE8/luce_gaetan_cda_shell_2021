#!/bin/bash

CHOIX=0
VAR1=0
VAR2=0


function Saisie_utilisateur() {
	echo "Entrer le nom de l'utilisateur"
	read VAR1
	echo "Le nom de l'utilisateur saisie est $VAR1"
}

function Verif_utilisateur() {
	VAR2=$(grep "^$VAR1" /etc/passwd | cut -d: -f1)
	[ -z $VAR2 ]
	if [ $? -eq 0 ]
	then
		echo "L'utilisateur $VAR1 n'existe pas"
		return 0
	else
		echo "L'utilisateur $VAR1 existe"
		return 1
	fi
}

echo "1 - Vérifier l'existence d'un utilisateur"
echo "2 - Connaitre l'uid d'un utilisateur"
echo "3 - Quitter"
echo "Faites votre choix en tapant le numéro correspondant dans le menu ci-dessus"

while [ $CHOIX -ne 9 ]
do
	echo "Saisir un choix dans le menu ci-dessus"
	read CHOIX
	case "$CHOIX" in
		1)
			Saisie_utilisateur
			Verif_utilisateur
			;;
		2)
			Saisie_utilisateur
			Verif_utilisateur
			if [ $? -ne 0 ]
			then
				echo "L'uid de l'utilisateur $VAR est :"
				grep "^$VAR1" /etc/passwd | cut -d: f3
			fi
			;;
		9)
			echo "Au revoir"
			;;
	esac
done



	
