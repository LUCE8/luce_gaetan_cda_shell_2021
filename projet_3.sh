#!/bin/bash
CHOIX=0
VAR1=0
VAR2=0

echo " 1 - Créer un nouvel utilisateur"
echo " 2 - Supprimer un utilisateur"

echo " Saisissez votre choix : "
read CHOIX
echo "Saisissez le nom de l'utilisateur : "
read VAR1

case "$CHOIX" in
	1)
		VAR2=$(grep "^$VAR1" /etc/passwd | cut -d: -f1)
		[ -z $VAR2 ]
		if [ $? -ne 0 ]
		then
			echo " L'utilisateur $VAR1 existe déja"
		else
			sudo adduser $VAR1
			echo "L'utilisateur $VAR1 a été créé"
		fi
		;;
	2)
		VAR2=$(grep "^$VAR1" /etc/passwd | cut -d: -f1)
		[ -z $VAR2 ]
		if [ $? -eq 0 ]
		then
			echo " L'utilisateur $VAR1 n'existe pas"
		else
			sudo userdel -r $VAR1
			echo "L'utilisateur $VAR1 a été supprimé"
		fi
		;;
esac

