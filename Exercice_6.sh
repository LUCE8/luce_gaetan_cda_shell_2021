#!/bin/bash

# ce programme, partant d'un nombre pris en arguments, affiche les autres nombres jusqu'à 1

A=$1

echo "Le nombre entré en argument est $A"
echo "La liste des nombres à partir de $A jusqu'à 1 est :"

for i in $(seq 1 $(($1-1)))
do
	A=$(($A-$i))
	echo $A
	A=$1
done
