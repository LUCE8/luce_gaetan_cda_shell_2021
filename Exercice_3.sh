#!/bin/bash

# Ce programme affiche le contenu d'un fichier pris en argument ligne par ligne

FICHIER=$1

while read line
do
	echo -e "$line"
done < $1
