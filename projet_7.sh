#!/bin/bash

VAR1=0
VAR2=0
VAR3=0
R=0
W=0
X=0
FICH=0

function Saisie_droits () {
	echo "Saisie des droits en lecture : entrer 1 si droits en lecture, 0 sinon"
	read VAR1
	echo "Saisie des droits en écriture : entrer 1 si droits en écriture, 0 sinon"
	read VAR2
	echo "Saisie des droits en exécution : entrer 1 si droits en exécution, 0 sinon"
	read VAR3
	if [ $VAR1 -eq 1 ]
	then
		R=4
	else
		R=0
	fi
	if [ $VAR2 -eq 1 ]
	then
		W=2
	else
		W=0
	fi
	if [ $VAR3 -eq 1 ]
	then
		X=1
	else
		X=0
	fi
}

echo "Saisir le nom du fichier dont les droits d'accès sont à modifier : "
read FICH
[ -e $FICH ]
if [ $? -eq 1 ]
then
	echo "Le fichier $FICH n'existe pas"
else
	echo "Modification des droits d'accès du propriétaire"
	Saisie_droits
	PR=$(($W+$R+$X))
	echo "Modification des droits d'accès du groupe"
	Saisie_droits
	GR=$(($W+$R+$X))
	echo "Modification des droits d'accès des autres utilisateurs"
	Saisie_droits
	AU=$(($W+$R+$X))
	chmod $PR$GR$AU $FICH
	echo "Les droits d'accès du propriétaire, du groupe et des autres utilisateurs pour le fichier $FICH ont été modifiés"
fi

	

	





		
