#!/bin/bash

echo "Ce programme permet de modifier l'utilisateur et le groupe auxquels appartient le fichier"
VAR1=0
VAR2=0
VAR3=0

echo "Entrer le nom du fichier"
read VAR1
[ -e $VAR1 ]
if [ $? -eq 1 ]
then
	echo "Le fichier $VAR1 n'existe pas"
else
	echo "Saisir le nom du nouveau propriétaire du fichier $VAR1"
	read VAR2
	echo "Saisir le nom du nouveau groupe du fichier $VAR1"
	read VAR3
	sudo chown $VAR2 $VAR1
	sudo chgrp $VAR3 $VAR1
	echo "L'utilisateur $VAR2 et le groupe $VAR3 sont les nouveaux propriétaires du fichier $VAR1"
fi

