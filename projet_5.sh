#!/bin/bash
CHOIX=0
VAR=0

echo "1 - Créer un fichier"
echo "2 - Supprimer un fichier"

read CHOIX

case "$CHOIX" in
	1)
		echo "Entrer le nom du fichier à créer :"
		read VAR
	       	[ -e $VAR ]
		if [ $? -eq 0 ]
		then
			echo "Le fichier $VAR  existe déjà"
		else
			touch $VAR
			echo "Le fichier $VAR1 a été créé"
		fi
		;;
	2)
		echo "Entrer le nom du fichier à supprimer :"
		read VAR
	       	[ -e $VAR ]
		if [ $? -eq 1 ]
		then
			echo "Le fichier $VAR  n'existe pas"
		else
			rm $VAR
			echo "Le fichier $VAR a été supprimé"
		fi
		;;
esac
