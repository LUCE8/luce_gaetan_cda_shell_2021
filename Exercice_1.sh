#!/bin/bash

# Vérification d'une date de naissance entrée en argument
# La date de naissance saisie doit être le 1er janvier 1981

PAR1=$1
PAR2=$2
PAR3=$3

JOUR=1er
MOIS=Janvier
ANNEE=1981

if [[ $PAR1 = $JOUR ]] && [[ $PAR2=$MOIS ]] && [[ $PAR3 -eq $ANNEE ]]
then
	echo "La date saisie $PAR1 $PAR2 $PAR3 est la bonne date de naissance"
else
	echo "La date saisie $PAR1 $PAR2 $PAR3 n'est pas la bonne date de naissance"
fi

# Verification du nombre de cadeaux reçus
KDO=0

echo "Saisir le nombre de cadeaux reçus : "
read KDO

if [[ $KDO -eq 5 ]] || [[ $KDO = cinq ]]
then
	echo "Le nombre de cadeaux reçus est bien égal à $KDO"
else
	echo "Le nombre de cadeaux n'est pas égal à $KDO"
fi

# Vérification du jour de naissance
NAISS=0

echo "Quel est votre jour de naissance ?"
read NAISS

if [ $NAISS = samedi ]
then
	echo "$NAISS est le bon jour de naissance"
else
	echo "$NAISS n'est pas le bon jour de naissance"
fi

