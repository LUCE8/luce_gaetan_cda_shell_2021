#!/bin/bash
CHOIX=0
VAR1=0
VAR2=0

echo "1 - Créer un nouveau groupe"
echo "2 - supprimer un groupe"

echo "Saisissez votre choix : "
read CHOIX
echo "Saisissez le numéro du groupe :"
read VAR1


VAR2=$(grep "^$VAR1" /etc/group | cut -d: -f1)
[ -z $VAR2 ]
echo $VAR2

case "$CHOIX" in 
	1)
		VAR2=$(grep "^$VAR1" /etc/group | cut -d: -f1)
		[ -z $VAR2 ]
		if [ $? -ne 0 ]
		then
			echo " Le groupe $VAR1 existe déjà"
		else
			sudo addgroup $VAR1
			echo "Le groupe $VAR1 a été créé"
		fi
		;;
	2)
		VAR2=$(grep "^$VAR1" /etc/group | cut -d: -f1)
		[ -z $VAR2 ]
		if [ $? -eq 0 ]
		then
			echo "Le groupe $VAR1 n'existe pas"
		else
			sudo delgroup $VAR1
			echo "Le groupe $VAR1 a été supprimé"
		fi
		;;
esac
