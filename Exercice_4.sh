#!/bin/bash

# Ce programme affiche les sous-répertoires du répertoire courant


for i in $(ls)
do
	if [ -d $i ]
	then
		echo $i
	fi
done
