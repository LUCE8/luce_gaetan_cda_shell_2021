#!/bin/bash

# Ce fichier répond à la question de l'exercice 5 sous forme de commentaires

# Dans le programme shell de l'exercice 5, 'who | grep $1 est une combinaison des deux commandes suivantes:
# - la commande who indiquant quels utilisateurs sont actuellement connectés au système;
# - la commande grep recherchant une chaîne de caractères dans un fichier. La chaîne de caractères, passée en arguments, est contenue dans la variable $1.

# Le programme prend donc en entrée le nom et le prénom d'un utilisateur.
# La commande w='who|grep $1' compare l'utilisateur contenu dans $1 à la liste des utilisateurs actuellement connectés ramenée par who.
# Si l'utilisateur n'est pas dans la liste des utilisateurs connectés, la variable w prend la valeur 0.
# Dans la 3ème ligne de commande, la valeur de w est testée par l'opérateur -z. Cet opérateur renvoie la valeur 0 si w est vide, 1 sinon.

# Nous avons donc un programme qui, prenant un utilisteur en argument, indique si celui-ci est actuellement connecté au système.
# La réponse complète renvoyée par le programme est la suivante : "$1 n'est pas connecté actuellement".

