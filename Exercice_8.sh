#!/bin/bash

TAB=( "19" "52" "683" "44" "5" "666" "847" "18" "19" "120" )
a=0
c=$((${#TAB[@]}-2))
j=0
d=1
e=0

echo "Liste des dix entiers non triée : ${TAB[@]}"

while ((d != 0))
do
	for i in $(seq 0 $c)
	do
		j=$(($i+1))
		if [ ${TAB[$i]} -gt ${TAB[$j]} ]
		then
			a=${TAB[$i]}
			TAB[$i]=${TAB[$j]}
			TAB[$j]=$a
			e=$(($e+1))
		fi
	done
	d=$e
	e=0
done

echo "Liste des dix entiers triée : ${TAB[@]}"
			




