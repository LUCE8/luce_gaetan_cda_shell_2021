#!/bin/bash

VAR1=0
VAR2=0

echo "Entrer le nom de l'utilisateur"
read VAR1

VAR2=$(grep "^$VAR1" /etc/passwd | cut -d: -f1)
[ -z $VAR2 ]

if [ $? -eq 0 ]
then
	echo "L'utilisateur $VAR1 n'existe pas"
else
	echo "L'utilisateur $VAR1 existe"
fi
