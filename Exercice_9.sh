#!/bin/bash

# Ce programme calcule et affiche le factoriel d'un nombre entré en argument
# La fonction permettant le calcul de factoriel du nombre passé en argument est récursive.

FAC=$1

function Factoriel () {
	local -i n
	if (( $1<=1 ))
	then
		echo 1
	else
		(( n=$1-1 ))
		n=$( Factoriel $n )
		echo $(($1*$n))
	fi
}
Factoriel $1



